<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>
<body class="nuestros_destinos">
<div>
	<div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    <a href="index.php" >
    	<br>
        <img src="img/logo.png" alt="Image" style="max-width:8%;">
    </a>
	</div>
<br>

<div class="container">
	<div class="row">
		<div class="col destino" align="center">
			<br>
			<br>
			<img src="img/destinos/titulo_nuestros_destinos.png" alt="Image" style="max-width:80%;">
		</div>
		<div class="col roques" align="center" data-toggle="modal" data-target="#roques">
		</div>
		<div class="col caracas" align="center" data-toggle="modal" data-target="#caracas">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col zulia" align="center" data-toggle="modal" data-target="#zulia">
		</div>
		<div class="col falcon" align="center" data-toggle="modal" data-target="#falcon">
		</div>
		<div class="col margarita" align="center" data-toggle="modal" data-target="#margarita">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col carabobo" align="center" data-toggle="modal" data-target="#carabobo">
		</div>
		<div class="col canaima" align="center" data-toggle="modal" data-target="#canaima">
		</div>
		<div class="col barranquilla" align="center" data-toggle="modal" data-target="#barranquilla">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col aruba" align="center" data-toggle="modal" data-target="#aruba">
		</div>
		<div class="col bonaire" align="center" data-toggle="modal" data-target="#bonaire">
		</div>
		<div class="col curazao" align="center" data-toggle="modal" data-target="#curazao">
		</div>
	</div>
</div>
</div>
<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->

<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>
  <!-- fin pie -->

  <!-- POPUP -->
<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="roques" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/Cuadros_Popup_LosRoques_31.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="caracas" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/Cuadros_Pop_up_Caracas_32.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="zulia" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/Cuadros_Popup_Zulia.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="falcon" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Falcon.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="margarita" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Margarita.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="carabobo" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/Cuadros_Pop_up_Caracas_32.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="canaima" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Canaima.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="barranquilla" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Barranquilla.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="aruba" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Aruba.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="bonaire" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Bonaire.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>

<div class="container">
	<div class="row">
            <!-- Button trigger modal -->            
            <!-- Modal -->
            <div class="modal fade" id="curazao" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <img src="img/popup/CuadrosPopup_Curazao.png" alt="Image" style="max-width:490px;">
                  </div>
                </div>
              </div>
            </div>
	</div>
</div>
  <!-- FIN POPUP -->
