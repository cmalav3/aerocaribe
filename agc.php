<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>
<body>
<!--menu-->
<div class="agc">
  <div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    <a href="index.php" >
    	<br>
        <img src="img/logo.png" alt="Image" style="max-width:8%;">
    </a>
	</div>
  <div>
    <br>
    <br>
    <br>
    <br>
    <br>
	<br>
    <br>
    <div class="col" align="center">
	<img src="img/agc/Img Titulo_Titulo Charter.png" alt="Image" style="max-width:30%;">
	</div>
	<br>
  </div>
</div>


<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->
<br>
<br>	
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<br>
			<p align="justify">
				Si deseas un servicio más personalizado y trasladarte en grupos, cuentas con nuestro servicio de vuelos chárter. 
				<br><br>Aerocaribe pone a disposición el servicio de vuelos privados con los más altos estándares de calidad, con el objetivo de satisfacer las necesidades particulares de cada cliente. Estos son ideales para el traslado de corporaciones, representantes de negocios, organizaciones deportivas, personalidades del estrellato, y demás interesados en tener un vuelo cómodo, seguro, puntual y con atención personalizada de primera categoría.
			</p>
		</div>
		<div class="col" align="center">
		  	<img src="img/agc/Img-Rosa.jpg" alt="Image" style="max-width:80%;">
		</div>
	</div>
</div>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<img src="img/agc/Img-Kitesurf.jpg" alt="Image" style="max-width:80%;">
		</div>
		<div class="col" align="center">
			<br>
			<br>
			<br>
		  	<p align="justify">
		  		Entre las ventajas que tienen los vuelos privados se encuentra el de disfrutar un servicio especializado y acorde a los anhelos de nuestros clientes, quienes además no tendrán que someterse a los horarios fijos de las rutas comerciales. Nos distinguimos de otras compañías que ofrecen también esta posibilidad, pues ofrecemos un servicio a bordo muy superior, así como las mejores tarifas del mercado nacional e internacional.
		  		Del mismo modo, le ofrecemos nuestro servicio de transporte aéreo en la modalidad de “Grupo” con tarifas especiales de acuerdo a las características de su grupo; entre las ventajas de esta opción está una mayor disponibilidad de asientos, flexibilidad en caso de necesitar alguna modificación antes de la emisión de los boletos; así como en el manejo del pago.
		  	</p>
		</div>
	</div>
</div>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<br>
			<br>
			<br>
		  	<p align="justify">
				Si desea saber más de estos servicios, realizar su solicitud o cotizar un Vuelo Chárter o Grupo con nosotros, puede escribir a <a class="titulo4"><strong>charter@aerocaribe.aero</strong></a>; le enviaremos su cotización en el menor tiempo posible, así como las condiciones aplicables a su viaje.
			</p>
			<a href="#">
		  		<button class="botones">Reserva Aquí</button>
		  	</a>
		</div>
		<div class="col" align="center">
			<img src="img/agc/Img-Grupo.jpg" alt="Image" style="max-width:80%;">
		</div>
	</div>
</div>
<br>
<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>
  <!-- fin pie -->
