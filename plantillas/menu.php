<nav id="nav">
		<ul class="main">
			<br>
				<li><a href=""><img src="img/menu/avion.png" alt="Image" style="max-width:8%;">&nbspNuestra Aerolínea</a>
					<ul class="main">
						<li><a href="nuestra_aerolinea.php">Misión, Visión y Valores</a></li>
					</ul>
					<ul class="main">
						<li><a href="nuestra_flota.php">Nuestra Flota</a></li>
					</ul>
					<ul class="main">
						<li><a href="nuestros_destinos.php">Nuestros Destinos</a></li>
					</ul>
				</li>
				<li><a href="#"><img src="img/menu/ubicacion.png" alt="Image" style="max-width:8%;">&nbspTu guía</a>
					<ul class="main">
						<li><a href="guia_destinos.php">Guía de Destinos</a></li>
					</ul>
					<ul class="main">
						<li><a href="guia.php">Guia de Viajeros</a></li>
					</ul>
				</li>
				<li><a href="agc.php"><img src="img/menu/charter.png" alt="Image" style="max-width:10%;">&nbspAerocaribe Grupos y Chárter</a></li>
				<li><a href="ac.php"><img src="img/menu/cargo.png" alt="Image" style="max-width:10%;">&nbspAerocaribe Cargo</a></li>
				<li><a href="#"><img src="img/menu/experiencia.png" alt="Image" style="max-width:10%;">&nbspExperiencia Aerocaribe</a></li>
				<li><a href="#"><img src="img/menu/contactanos.png" alt="Image" style="max-width:10%;">&nbspContáctenos</a></li>
		</ul>
</nav>
<div class="overlay"></div>

<script>
$('.menu, .overlay').click(function () {
	$('.menu').toggleClass('clicked');
	
	$('#nav').toggleClass('show');
	
});
</script>
</div>