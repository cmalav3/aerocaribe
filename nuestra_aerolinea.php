<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>
<body>
<!--menu-->
<div class="nuestra_aerolinea">
  	<div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    	<a href="index.php" >
    	<br>
        	<img src="img/logo.png" alt="Image" style="max-width:8%;">
   		 </a>
	</div>
    <br>
    <br>
    <br>
    <br>
    <br>
	<br>
    <br>
<div class="container">
	<div class="row">
		<div class="col-md-3" align="center">
		</div>
		<div class="col-md-6" align="center">
			<h1 class="titulo_principal">Nuestra Aerolinea</h1>
			<h1 class="raya_titulo"></h1>
		</div>
		<div class="col-md-3" align="center">
		</div>
	</div>
</div>
	<br>
  </div>

<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->
<br>
<br>

<div class="container">
	<div class="row" align="center">
		<div class="col-md-4 cuadro">
			<p align="center" id='titulo_azul'><strong>
		  		Misión</strong>
		  	</p>
		  	<p align="justify" id='parrafo'>
		  		Prestar a nuestros clientes un servicio de transporte aéreo comercial de pasajeros y carga, ofreciendo una experiencia de vuelo nacional e internacional con calidad, seguridad y puntualidad que contribuya con el desarrollo social y turístico del país.
		  	</p>
		</div>
		<div class="col-md-4 cuadro">
			<p align="center" id='titulo_azul'><strong>
		  		Visión</strong>
		  	</p>
		  	<p align="justify" id='parrafo'>
		  		Consolidarnos como la mejor aerolinea de Venezuela hacia todo el Caribe, ofreciendo vuelos comerciales con un servicio de excelente calidad, adaptándonos a la actualidad del mercado y cubriendo sus necesidades
		  	</p>
		</div>

		<div class="col-md-4 cuadro">
			<p align="center" id='titulo_azul'><strong>
			  		Valores</strong>
			</p>
			<div class="row">  	
			  	<div class="col-md-6">
				  	<p align="left" id='parrafo'>
						•Trabajo en equipo<br>
						•Servicio al cliente<br>
						•Compromiso
				  	</p>
		  		</div>
		  		<div class="col-md-6">
				  	<p align="left" id='parrafo'>
						•Excelencia <br>
						•Seguridad
				  	</p>
		  		</div>
		  	</div>
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="valores">
	<div class="row">
		<div class="col-md-12" align="center">
			<img src="img/nuestra_aerolinea/Img_Banner_Valores.jpg" alt="Image" style="max-width:100%;">
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="container">
	<div class="row" align="center">
		<div class="col-md-3">
		  	<p align="justify" id='parrafo'>
				• Administración<br><br>
				• Contabilidad<br><br>
				• Carga
			</p>
		</div>
		<div class="col-md-3">
		  	<p align="justify" id='parrafo'>
				• Comercialización<br><br>
				• Compras<br><br>
				• Diseño
			</p>
		</div>

		<div class="col-md-3">
		  	<p align="justify" id='parrafo'>
				• Jefatura de intrucción<br><br>
				• Mantenimiento<br><br>
				• Mercadeo
			</p>
		</div>
		<div class="col-md-3">
		  	<p align="justify" id='parrafo'>
				• Operaciones<br><br>
				• Recursos Humanos<br><br>
				• Tesoreria
			</p>
		</div>
	</div>
</div>
<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>
  <!-- fin pie -->
