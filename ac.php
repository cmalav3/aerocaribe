<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>
<body>
<!--menu-->
<div class="ac">
	<div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    <a href="index.php" >
    	<br>
        <img src="img/logo.png" alt="Image" style="max-width:8%;">
    </a>
	</div>
<br>
<div>
	<br>
	<div class="row">
		<div class="col-md-4" align="right">
			<img src="img/ac/Img_Titulo_Cargo.png" alt="Image" style="max-width:50%;">
		</div>
	</div>
</div>
<br>
<br>
<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->
<br>

<div class="container">
	<div class="row">
		<div class="col" align="right">
			<a href="ac.php">
		  		<button class="botones1">Carga Nacional</button>
		  	</a>
		</div>
		<div class="col" align="left">
			<a href="ci.php">
		  		<button class="botones1">Carga Internacional</button>
			</a>
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<p class="titulo1"><strong>
				TARIFAS DE CARGA</strong>
			</p>
		  	<p>
				Nuestra tarifas para él envió de carga nacional varía dependiendo de la oferta y la demanda.

				TIPOS DE CARGA

				Nosotros podemos transportar carga general, carga perecedera, equipaje no acompañado.

				CONDICIONES

				Ningún bulto puede pesar más de 25 kilos y deben tener una medida máximas de 50 cm de largo / 50 cm de ancho / 100 cm de alto.

				CARGA ESPECIAL ACOMPAÑADA

				Podremos manejar cargas especiales (HUM, PER, AVI). Te invitamos a ver nuestras condiciones para el transporte de carga haciendo click aquí.
		  	</p>
		</div>
		<div class="col" align="center">
		  	<img src="img/ac/Img_Caja.png" alt="Image" style="max-width:60%;">
		</div>
	</div>
</div>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
		  	<img src="img/ac/Img_Kennel.png" alt="Image" style="max-width:60%;">
		</div>
		<div class="col" align="center">
		  	<img src="img/ac/Img_Texto_animales_vivos.png" alt="Image" style="max-width:80%;">
		</div>
	</div>
</div>

<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>