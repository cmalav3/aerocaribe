<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>

<body>
<!--menu-->
<div class="fondo">
	<div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    <a href="index.php" >
      <br>
        <img src="img/logo.png" alt="Image" style="max-width:8%;">
    </a>
	</div>
</div>
<br>
<br>

<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->

<div class="slogan col-md-12" align="center">
    <h3 class="titulo3" align="center"><strong>VUELA CON NOSOTROS</strong></h3> 
</div>
<br>
<div class="container" align="center">
  <div class="row blog">
    <div class="col-md-12">
      <div id="blogCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#blogCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Carousel items -->
        <div class="carousel-inner">

          <div class="carousel-item active">
              <div class="row">
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-01.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-02.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-03.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-04.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
              </div>
              <!--.row-->
          </div>
            <!--.item-->

          <div class="carousel-item">
              <div class="row">
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-01.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-02.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-03.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
                  <div class="col-md-3">
                      <a href="#">
                          <img src="img/ventanas/Ventanas-04.png" alt="Image" style="max-width:100%;">
                      </a>
                  </div>
              </div>
              <!--.row-->
          </div>
            <!--.item-->
        </div>
          <!--.carousel-inner-->
      </div>
      <!--.Carousel-->
    </div>
  </div>
</div>
<br>
<br>

<div class="bordo">
  <div >
    <br>
    <br>
    <br>
    <br>
    <br>
        <h1 class="blanco" align="center">BIENVENIDO A BORDO</h1> 
        <p class="blanco" align="center">EN AEROCARIBE SABEMOS QUE EL VIAJE COMIENZA EN EL AEROPUERTO</p> 
    <br>

    <div align="center">
    <button class="btn btn-info btn-responsive btninter">CONOCE NUESTROS SERVICIOS</button>
    </div>

    <br>
    <br>
    <br>
    <br>

  </div>
</div>

<br>

<div>
    <h3 class="titulo3" align="center"><strong>ITINERARIOS</strong></h3> 
</div>

<div class="container">
  <div id="demo" class="carousel slide" data-ride="carousel">
    <!-- The slideshow -->
    <div class="carousel-inner no-padding my-5">

      <div class="carousel-item active">
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
      </div>

      <div class="carousel-item">
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
      </div>

      <div class="carousel-item">
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
          <a href="#">
              <img src="img/itinerario.png" alt="Image" style="max-width:100%;">
          </a>
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
       <span class="carousel-control-prev-icon sp"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon sp"></span>
    </a>
  </div>
</div>

<div class="correo">
  <div>
    <br>
    <div>
  <div class="row">
    <div class="col-md-6" align="center">
    </div>
    <div class="col-md-3" align="center">
        <h5 class="blanco" align="center">¡Queremos conocerte mejor!</h5> 
        <p class="blanco" align="center">Ayúdanos, proporcionandonos un poco de informacion para mejorar nuestros servicios y asi recibirás excelente ofertas </p>
    </div>
    <div class="col-md-3" align="center">
    </div>
  </div>
  <div class="row">
    <div class="col-md-6" align="center">
    </div>
    <div class="col-md-3" align="center">
      <button class="btn btn-info btn-responsive btninter">SOLICITAR INFORMACIÓN</button>
    </div>
    <div class="col-md-3" align="center">
    </div>
  </div>
</div>
    <br>
    <br>
    <br>
    <br>

  </div>
</div>

<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>
  <!-- fin pie -->