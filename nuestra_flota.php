<?php
include_once 'plantillas/head.php';
include_once 'plantillas/social.php';
?>

<body>
<div class="nuestra_flota">
	<div class="col-md-12" align="center">
		<div class="menu"> <span></span> 
		</div>
    <a href="index.php" >
    	<br>
        <img src="img/logo.png" alt="Image" style="max-width:8%;">
    </a>
	</div>
<br>
<div class="container">
	<div class="row">
		<br>
		<div class="col-md-5" align="left">
			<h1 class="titulo_principal">Nuestra <br>Flota</h1>
			<h1 class="raya_titulo_flota"></h1>
		</div>
		<div class="col-md-1" align="left">
			<br>
			<img src="img/nuestra_flota/Img_Letras_L410.png" alt="Image" style="max-width:70%;">
		</div>
		<div class="col-md-6" align="center">
			<div class="container cuadro_parrafo">
			  	<p align="justify">
			  		<a class="titulo3">DESEMPEÑO OPERACIONAL</a><br>
				<a class="titulo4"><strong>CAPACIDAD DE DESPEGUE Y ATERRIZAJE CORTO</strong></a><br>
					Con el diseño único de la L410, la aeronave puede operar en cualquier lugar, necesitando sólo unos pocos cientos de metros de pista de aterrizaje con una  fuerza mínima de
					6 kg/cm cuadrados (85 Ib/sq.in).
					<br>
					<br>
					<a href="#" onclick="vermas('mas');" id="mas" class="boton_mas_menos"><button class="btn btn-info btn-responsive btninter">LEER MAS</button></a>
				</p>
				<p id="desplegar" style="display:none;" align="justify">
					<a class="titulo4"><strong>CONFIABLE Y SEGURO</strong></a><br>
					Más del 80% de todos los aviones L410 jamás producidos todavía están en servicio. Todo es metal airframe garantiza la máxima vida útil. Además, una considerable rigidez y
					resistencia de la estructura, así como una alta resistencia a la corrosión y una larga vida útil de los componentes, garantizan el más alto nivel de fiabilidad operativa. Durante más de 30 años, el L410 ha demostrado su excepcional calidad y fiabilidad.
					<br><br>
					<a class="titulo4"><strong>DE ALTA CAPACIDAD</strong></a><br>

					El avión ha demostrado un servicio consistente y confiable en una gama excepcionalmente amplia de temperaturas y condiciones climáticas, desde sabanas africanas polvorientas y secas hasta la selva amazónica lluviosa y desde el nivel del mar hasta destinos montañosos "calientes y altos".
					<br>
					<br>
					<a href="#" onclick="vermas('menos');" id="menos"><button class="btn btn-info btn-responsive btninter">LEER MENOS</button></a>
					<br>
				</p>
				<br>
			</div>
		</div>
	</div>
</div>
</div>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-12" align="center">
		  	<p align="center" id='titulo_azul'><strong>
		  		CARACTERISTICAS:</strong>
		  	</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" align="center">
		  	<p align="left" id='parrafo'>
		  		<strong>·Clase:</strong> Bimotor Mixto (Pax-Carga)<br>
		  		<strong>·Velocidad de Crucero:</strong> 180 Nudos (335 Km/Hora)<br>
		  		<strong>·Carga máxima:</strong> 1.500kg./18m3
		  	</p>
		</div>
		<div class="col-md-6" align="center">
		  	<p align="left" id='parrafo'>
		  		<strong>·Capacidad Pasajeros:</strong> 15+2 tripulantes<br>
		  		<strong>·Techo Máximo:</strong> 15.000 Pies<br>
		  		<strong>·Autonomía de Vuelo:</strong> 3:00 Horas/550 millas náuticas.
		  	</p>
		</div>
	</div>
</div>

<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<img src="img/nuestra_flota/cabina_texto.jpg" alt="Image" style="max-width:100%;">
		</div>
		<div class="col" align="center">
		  	<img src="img/nuestra_flota/cabina.jpg" alt="Image" style="max-width:100%;">
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
		  	<img src="img/nuestra_flota/infogramas.png" alt="Image" style="max-width:100%;">
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col" align="center">
			<img src="img/nuestra_flota/modificaciones.jpg" alt="Image" style="max-width:100%;">
		</div>
		<div class="col" align="center">
		  	<img src="img/nuestra_flota/modificaciones_texto.jpg" alt="Image" style="max-width:100%;">
		</div>
	</div>
</div>

<!--menu-->
<?php 
include_once 'plantillas/menu.php';
?>
<!--fin menu-->

<!-- pie -->
<?php 
include_once 'plantillas/pie.php';
?>
  <!-- fin pie -->